import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RelatorioPage } from './relatorio/relatorio.page';
import { CadastroPage } from './cadastro/cadastro.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaPage } from './lista/lista.page';

const routes: Routes = [
  {
    path: '', children: [
      {path: 'pagar', component: ListaPage},
      {path: 'receber', component: ListaPage},
      {path: 'cadastro', component: CadastroPage},
      {path: 'relatorio', component: RelatorioPage},
    ]
  },
  {
    path: 'lista',
    loadChildren: () => import('./lista/lista.module').then( m => m.ListaPageModule)
  }

];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    ListaPage,
    CadastroPage,
    RelatorioPage
  ],
  exports: [RouterModule]
})
export class ContasRoutingModule { }
