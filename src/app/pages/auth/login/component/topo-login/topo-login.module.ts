import { TopoLoginComponent } from './topo-login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [TopoLoginComponent ],
  imports: [
    CommonModule

  ],
  exports: [TopoLoginComponent]
})
export class TopoLoginModule { }
