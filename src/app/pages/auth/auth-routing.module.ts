import { TopoLoginComponent } from './login/component/topo-login/topo-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RegisterPage } from './register/register.page';
import { ForgotPage } from './forgot/forgot.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from './login/login.page';

const routes: Routes = [
  {
    path: '', children:[
      {path: '', component: LoginPage},
      {path: 'forgot', component: ForgotPage},
      {path: 'register', component: RegisterPage},
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: [
    LoginPage,
    ForgotPage,
    RegisterPage,
    TopoLoginComponent
  ]
})
export class AuthRoutingModule { }
