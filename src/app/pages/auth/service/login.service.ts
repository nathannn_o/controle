import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: any;
  createUser(user: any) {
    
    this.auth.createUserWithEmailAndPassword(user.email, user.password).
    then(credentials => console.log(credentials));
    throw new Error('Method not implemented.');
  }
  login(email: any, password: any) {
    this.auth.signInWithEmailAndPassword(email, password).
    then(() => this.nav.navigateForward('home')).
    catch (() => this.toastMessage('Dados de acesso incorretos', 'danger'));
  }
  logout(){
    this.auth.signOut().
    then(() => this.nav.navigateBack('auth'));
  }
  recoverPass(data){
    this.auth.sendPasswordResetEmail(data.email).
    then(() => this.nav.navigateBack('auth')).
    catch(err => {
      console.log(err);
    });
  }

  constructor(    
    private nav: NavController,
    private auth: AngularFireAuth,
    private toastCtrl: ToastController,
    ) { }

  private toastMessage(message: string, color: string): Promise<HTMLIonToastElement> {
     return this.toastCtrl.create({
        message,
        duration: 1500,
       color
     });
  }
  
}
